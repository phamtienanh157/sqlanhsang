package com.mycompany.myapp.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Lop.
 */
@Entity
@Table(name = "lop")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Lop implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ten_lop", nullable = false)
    private String tenLop;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Lop id(Long id) {
        this.id = id;
        return this;
    }

    public String getTenLop() {
        return this.tenLop;
    }

    public Lop tenLop(String tenLop) {
        this.tenLop = tenLop;
        return this;
    }

    public void setTenLop(String tenLop) {
        this.tenLop = tenLop;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Lop)) {
            return false;
        }
        return id != null && id.equals(((Lop) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Lop{" +
            "id=" + getId() +
            ", tenLop='" + getTenLop() + "'" +
            "}";
    }
}
