package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Lop;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Lop entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LopRepository extends JpaRepository<Lop, Long> {}
