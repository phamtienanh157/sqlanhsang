package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.domain.Lop;
import com.mycompany.myapp.repository.LopRepository;
import com.mycompany.myapp.service.LopService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Lop}.
 */
@Service
@Transactional
public class LopServiceImpl implements LopService {

    private final Logger log = LoggerFactory.getLogger(LopServiceImpl.class);

    private final LopRepository lopRepository;

    public LopServiceImpl(LopRepository lopRepository) {
        this.lopRepository = lopRepository;
    }

    @Override
    public Lop save(Lop lop) {
        log.debug("Request to save Lop : {}", lop);
        return lopRepository.save(lop);
    }

    @Override
    public Optional<Lop> partialUpdate(Lop lop) {
        log.debug("Request to partially update Lop : {}", lop);

        return lopRepository
            .findById(lop.getId())
            .map(
                existingLop -> {
                    if (lop.getTenLop() != null) {
                        existingLop.setTenLop(lop.getTenLop());
                    }

                    return existingLop;
                }
            )
            .map(lopRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lop> findAll() {
        log.debug("Request to get all Lops");
        return lopRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Lop> findOne(Long id) {
        log.debug("Request to get Lop : {}", id);
        return lopRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Lop : {}", id);
        lopRepository.deleteById(id);
    }
}
