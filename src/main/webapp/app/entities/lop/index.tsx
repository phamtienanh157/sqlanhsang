import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Lop from './lop';
import LopDetail from './lop-detail';
import LopUpdate from './lop-update';
import LopDeleteDialog from './lop-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={LopUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={LopUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LopDetail} />
      <ErrorBoundaryRoute path={match.url} component={Lop} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={LopDeleteDialog} />
  </>
);

export default Routes;
