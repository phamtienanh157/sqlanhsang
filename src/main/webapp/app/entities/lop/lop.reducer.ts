import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ILop, defaultValue } from 'app/shared/model/lop.model';

export const ACTION_TYPES = {
  FETCH_LOP_LIST: 'lop/FETCH_LOP_LIST',
  FETCH_LOP: 'lop/FETCH_LOP',
  CREATE_LOP: 'lop/CREATE_LOP',
  UPDATE_LOP: 'lop/UPDATE_LOP',
  PARTIAL_UPDATE_LOP: 'lop/PARTIAL_UPDATE_LOP',
  DELETE_LOP: 'lop/DELETE_LOP',
  RESET: 'lop/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ILop>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type LopState = Readonly<typeof initialState>;

// Reducer

export default (state: LopState = initialState, action): LopState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_LOP_LIST):
    case REQUEST(ACTION_TYPES.FETCH_LOP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_LOP):
    case REQUEST(ACTION_TYPES.UPDATE_LOP):
    case REQUEST(ACTION_TYPES.DELETE_LOP):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_LOP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_LOP_LIST):
    case FAILURE(ACTION_TYPES.FETCH_LOP):
    case FAILURE(ACTION_TYPES.CREATE_LOP):
    case FAILURE(ACTION_TYPES.UPDATE_LOP):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_LOP):
    case FAILURE(ACTION_TYPES.DELETE_LOP):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LOP_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LOP):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_LOP):
    case SUCCESS(ACTION_TYPES.UPDATE_LOP):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_LOP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_LOP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/lops';

// Actions

export const getEntities: ICrudGetAllAction<ILop> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_LOP_LIST,
  payload: axios.get<ILop>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<ILop> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_LOP,
    payload: axios.get<ILop>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ILop> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_LOP,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ILop> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_LOP,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<ILop> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_LOP,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ILop> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_LOP,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
