export interface ILop {
  id?: number;
  tenLop?: string;
}

export const defaultValue: Readonly<ILop> = {};
