import dayjs from 'dayjs';
import { ILop } from 'app/shared/model/lop.model';
import { IDiem } from 'app/shared/model/diem.model';
import { Sex } from 'app/shared/model/enumerations/sex.model';

export interface ISinhVien {
  id?: number;
  maSinhVien?: string;
  password?: string;
  hoTen?: string;
  ngaySinh?: string | null;
  gioiTinh?: Sex | null;
  email?: string | null;
  soDienThoai?: string | null;
  nganh?: string;
  khoaHoc?: string;
  lop?: ILop | null;
  diems?: IDiem[] | null;
}

export const defaultValue: Readonly<ISinhVien> = {};
