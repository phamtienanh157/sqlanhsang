package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Lop;
import com.mycompany.myapp.repository.LopRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LopResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class LopResourceIT {

    private static final String DEFAULT_TEN_LOP = "AAAAAAAAAA";
    private static final String UPDATED_TEN_LOP = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/lops";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LopRepository lopRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLopMockMvc;

    private Lop lop;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Lop createEntity(EntityManager em) {
        Lop lop = new Lop().tenLop(DEFAULT_TEN_LOP);
        return lop;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Lop createUpdatedEntity(EntityManager em) {
        Lop lop = new Lop().tenLop(UPDATED_TEN_LOP);
        return lop;
    }

    @BeforeEach
    public void initTest() {
        lop = createEntity(em);
    }

    @Test
    @Transactional
    void createLop() throws Exception {
        int databaseSizeBeforeCreate = lopRepository.findAll().size();
        // Create the Lop
        restLopMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(lop)))
            .andExpect(status().isCreated());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeCreate + 1);
        Lop testLop = lopList.get(lopList.size() - 1);
        assertThat(testLop.getTenLop()).isEqualTo(DEFAULT_TEN_LOP);
    }

    @Test
    @Transactional
    void createLopWithExistingId() throws Exception {
        // Create the Lop with an existing ID
        lop.setId(1L);

        int databaseSizeBeforeCreate = lopRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLopMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(lop)))
            .andExpect(status().isBadRequest());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTenLopIsRequired() throws Exception {
        int databaseSizeBeforeTest = lopRepository.findAll().size();
        // set the field null
        lop.setTenLop(null);

        // Create the Lop, which fails.

        restLopMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(lop)))
            .andExpect(status().isBadRequest());

        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllLops() throws Exception {
        // Initialize the database
        lopRepository.saveAndFlush(lop);

        // Get all the lopList
        restLopMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lop.getId().intValue())))
            .andExpect(jsonPath("$.[*].tenLop").value(hasItem(DEFAULT_TEN_LOP)));
    }

    @Test
    @Transactional
    void getLop() throws Exception {
        // Initialize the database
        lopRepository.saveAndFlush(lop);

        // Get the lop
        restLopMockMvc
            .perform(get(ENTITY_API_URL_ID, lop.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(lop.getId().intValue()))
            .andExpect(jsonPath("$.tenLop").value(DEFAULT_TEN_LOP));
    }

    @Test
    @Transactional
    void getNonExistingLop() throws Exception {
        // Get the lop
        restLopMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewLop() throws Exception {
        // Initialize the database
        lopRepository.saveAndFlush(lop);

        int databaseSizeBeforeUpdate = lopRepository.findAll().size();

        // Update the lop
        Lop updatedLop = lopRepository.findById(lop.getId()).get();
        // Disconnect from session so that the updates on updatedLop are not directly saved in db
        em.detach(updatedLop);
        updatedLop.tenLop(UPDATED_TEN_LOP);

        restLopMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedLop.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedLop))
            )
            .andExpect(status().isOk());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeUpdate);
        Lop testLop = lopList.get(lopList.size() - 1);
        assertThat(testLop.getTenLop()).isEqualTo(UPDATED_TEN_LOP);
    }

    @Test
    @Transactional
    void putNonExistingLop() throws Exception {
        int databaseSizeBeforeUpdate = lopRepository.findAll().size();
        lop.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLopMockMvc
            .perform(
                put(ENTITY_API_URL_ID, lop.getId()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(lop))
            )
            .andExpect(status().isBadRequest());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLop() throws Exception {
        int databaseSizeBeforeUpdate = lopRepository.findAll().size();
        lop.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLopMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(lop))
            )
            .andExpect(status().isBadRequest());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLop() throws Exception {
        int databaseSizeBeforeUpdate = lopRepository.findAll().size();
        lop.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLopMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(lop)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLopWithPatch() throws Exception {
        // Initialize the database
        lopRepository.saveAndFlush(lop);

        int databaseSizeBeforeUpdate = lopRepository.findAll().size();

        // Update the lop using partial update
        Lop partialUpdatedLop = new Lop();
        partialUpdatedLop.setId(lop.getId());

        partialUpdatedLop.tenLop(UPDATED_TEN_LOP);

        restLopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLop.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLop))
            )
            .andExpect(status().isOk());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeUpdate);
        Lop testLop = lopList.get(lopList.size() - 1);
        assertThat(testLop.getTenLop()).isEqualTo(UPDATED_TEN_LOP);
    }

    @Test
    @Transactional
    void fullUpdateLopWithPatch() throws Exception {
        // Initialize the database
        lopRepository.saveAndFlush(lop);

        int databaseSizeBeforeUpdate = lopRepository.findAll().size();

        // Update the lop using partial update
        Lop partialUpdatedLop = new Lop();
        partialUpdatedLop.setId(lop.getId());

        partialUpdatedLop.tenLop(UPDATED_TEN_LOP);

        restLopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLop.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLop))
            )
            .andExpect(status().isOk());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeUpdate);
        Lop testLop = lopList.get(lopList.size() - 1);
        assertThat(testLop.getTenLop()).isEqualTo(UPDATED_TEN_LOP);
    }

    @Test
    @Transactional
    void patchNonExistingLop() throws Exception {
        int databaseSizeBeforeUpdate = lopRepository.findAll().size();
        lop.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, lop.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(lop))
            )
            .andExpect(status().isBadRequest());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLop() throws Exception {
        int databaseSizeBeforeUpdate = lopRepository.findAll().size();
        lop.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(lop))
            )
            .andExpect(status().isBadRequest());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLop() throws Exception {
        int databaseSizeBeforeUpdate = lopRepository.findAll().size();
        lop.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLopMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(lop)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Lop in the database
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLop() throws Exception {
        // Initialize the database
        lopRepository.saveAndFlush(lop);

        int databaseSizeBeforeDelete = lopRepository.findAll().size();

        // Delete the lop
        restLopMockMvc.perform(delete(ENTITY_API_URL_ID, lop.getId()).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Lop> lopList = lopRepository.findAll();
        assertThat(lopList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
