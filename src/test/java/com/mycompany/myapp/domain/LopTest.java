package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LopTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Lop.class);
        Lop lop1 = new Lop();
        lop1.setId(1L);
        Lop lop2 = new Lop();
        lop2.setId(lop1.getId());
        assertThat(lop1).isEqualTo(lop2);
        lop2.setId(2L);
        assertThat(lop1).isNotEqualTo(lop2);
        lop1.setId(null);
        assertThat(lop1).isNotEqualTo(lop2);
    }
}
